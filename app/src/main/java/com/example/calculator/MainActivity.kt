package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    var calculation = StringBuilder()
    var arithmeticOperations = ArrayList<String>()
    lateinit var calculationText: TextView
    lateinit var finalCalculationText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        calculationText = findViewById(R.id.calculation_text)
        finalCalculationText = findViewById(R.id.final_result)
    }

    fun calculateBtnClick(view: View) {
        var calculationStringFinal:Double = 0.0
        var calculationString = calculation.toString()
        val numbers = calculationString.split("+", "/", "*", "-").map { it.toString().toDouble() }

        var loopIndex:Int = 0
        for(number in numbers) {
            if(loopIndex == 0) {
                calculationStringFinal += number
            } else {
                when(arithmeticOperations[loopIndex - 1]) {
                    "+" -> {
                        calculationStringFinal += number
                    }
                    "*" -> {
                        calculationStringFinal *= number
                    }
                    "/" -> {
                        calculationStringFinal /= number
                    }
                    "-" -> {
                        calculationStringFinal -= number
                    }
                }
            }

            loopIndex++
        }

        finalCalculationText.text = calculationStringFinal.toString()
    }

    fun buttonArithmaticOnClick(view: View) {
        when(view.id) {
            R.id.button_add -> {
                calculation.append("+")
                arithmeticOperations.add("+")
                calculationText.text = calculation.toString()
            }
            R.id.button_division -> {
                calculation.append("/")
                arithmeticOperations.add("/")
                calculationText.text = calculation.toString()
            }
            R.id.button_multiply -> {
                calculation.append("*")
                arithmeticOperations.add("*")
                calculationText.text = calculation.toString()
            }
            R.id.button_minus -> {
                calculation.append("-")
                arithmeticOperations.add("-")
                calculationText.text = calculation.toString()
            }
        }
    }

    fun buttonNumberClick(view: View) {
        when(view.id) {
            R.id.button_period -> {
                calculation.append(".")
                calculationText.text = calculation.toString()
            }
            R.id.button_0 -> {
                calculation.append("0")
                calculationText.text = calculation.toString()
            }
            R.id.button_1 -> {
                calculation.append("1")
                calculationText.text = calculation.toString()
            }
            R.id.button_2 -> {
                calculation.append("2")
                calculationText.text = calculation.toString()
            }
            R.id.button_3 -> {
                calculation.append("3")
                calculationText.text = calculation.toString()
            }
            R.id.button_4 -> {
                calculation.append("4")
                calculationText.text = calculation.toString()
            }
            R.id.button_5 -> {
                calculation.append("5")
                calculationText.text = calculation.toString()
            }
            R.id.button_6 -> {
                calculation.append("6")
                calculationText.text = calculation.toString()
            }
            R.id.button_7 -> {
                calculation.append("7")
                calculationText.text = calculation.toString()
            }
            R.id.button_8 -> {
                calculation.append("8")
                calculationText.text = calculation.toString()
            }
            R.id.button_9 -> {
                calculation.append("9")
                calculationText.text = calculation.toString()
            }
        }
    }

    fun buttonClearCalculator(view: View) {
        calculation.clear()
        calculationText.text = calculation.toString()
        finalCalculationText.text = ""
    }


}